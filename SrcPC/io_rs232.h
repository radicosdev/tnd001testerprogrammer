/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file io_rs232.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/
//---------------------------------------------------------------------------
//  For rs232 data communication

#define RS_BUFSIZ   256

//COM11 for example
extern int      		com11_port;
extern char            com11_RxBuff [RS_BUFSIZ +1];
extern unsigned char   com11_RxCnt;
extern char*           com11_RxPutPtr;
extern char*           com11_RxGetPtr;
extern int             com11_TimeOut;

extern int             com12_port;

extern int             com13_port;

extern int             com14_port;

extern int             com15_port;

extern int             com16_port;

extern int             com18_port;

extern int             com19_port;


//COM11 for example
int com11_open(void);
int com11_TransmitChar(char TxChar);
int com11_ReceiveChar(void);
void CVICALLBACK com11_Rx_CB (int portNo,int eventMask,void *callbackData);

int com12_open(void);

int com13_open(void);

int com14_open(void);

int com15_open(void);

int com16_open(void);

int com18_open(void);

int com19_open(void);


int com_close(int TheCom);
