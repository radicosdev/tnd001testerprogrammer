#Initialize serial device
import serial
import serial.tools.list_ports

import time
import serial
import logging
 
# Create and configure logger
logging.basicConfig(filename="newfile.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
 
# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)
 
import configparser
#import os
config = configparser.ConfigParser()

##Get the absolute path of ini file by doing os.getcwd() and joining it to config.ini
#ini_path = os.path.join(os.getcwd(),'TestPlan.ini')
#config.read(ini_path)
config.read('TestPlan.ini')
keys = [
    "ProductID",
    "TestPlanID",
    "TestLogFilePath",
    "LogFileEXT",
    "LogWithAddOnInfos",  
    "EnableExtraCtrl",
    "FirmwarePath",
    "ProgCmdPath", 
    "ProgCmdLogFileName"
]

keys1 = [
    "State", 
    "TestName", 
    "MinVal", 
    "MaxVal", 
    "Remark"
]

ch2 = config.get('Test36','TestName')
ch3 = config.get('Test37','TestName')
ch4 = config.get('Test38','TestName')
print(ch2,  ch3,  ch4)

#TO DO: make a dictionary to put all the values and define a max number of tests and after make a for loop to test that all these tests will be in the gui

for key in keys:
    try:
        value1 = config.get("Setup", key)
        print(f"{key}:", value1)
    except configparser.NoOptionError:
        print(f"No option '{key}' in section 'SETTINGS'")

for key in keys1:
    
    try:
        value2 = config.get("Test1", key)
        print(f"{key}:", value2)
    except configparser.NoOptionError:
        print(f"No option '{key}' in section 'SETTINGS'")
    try:
        value3 = config.get("Test2", key)
        print(f"{key}:", value3)
    except configparser.NoOptionError:
        print(f"No option '{key}' in section 'SETTINGS'")
        
#    try:
#        value3 = config.get("Test2", key)
#        print(f"{key}:", value3)
#    except configparser.NoOptionError:
#        print(f"No option '{key}' in section 'SETTINGS'")        
#    try:
#        value3 = config.get("Test2", key)
#        print(f"{key}:", value3)
#    except configparser.NoOptionError:
#        print(f"No option '{key}' in section 'SETTINGS'")        


#print(config['DEFAULT']['path'])     # -> "/path/name/"
#config['DEFAULT']['path'] = '/var/shared/'    # update
#config['DEFAULT']['default_message'] = 'Hey! help me!!'   # create

#with open('FILE.INI', 'w') as configfile:    # save
#    config.write(configfile)
    
## Test messages
#logger.debug("Harmless debug Message") #logger.info("Just an information") #logger.warning("Its a Warning") #logger.error("Did you try to divide by zero")#logger.critical("Internet is down")

def USB_Init():
        MUX1 = serial.Serial('COM11', baudrate= 2400,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        MUX1.flush() # clearing the input buffer
        if not MUX1.isOpen():
            logger.error("Com11 cannot be openned\r")
        #MUX1.close()
        #if not MUX1.isOpen():
        #    MUX1.open()
        #print('com1 is open', MUX1.isOpen())
        MUX2 = serial.Serial('COM12', baudrate= 2400,  bytesize = 8, parity =  serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        MUX2.flush()
        if not MUX2.isOpen():
            logger.error("Com12 cannot be openned\r")
        #MUX2.close()
        MUX3 = serial.Serial('COM13', baudrate= 2400,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        MUX3.flush()
        if not MUX3.isOpen():
            logger.error("Com13 cannot be openned\r")
        #MUX3.close()
        MUX4 = serial.Serial('COM14', baudrate= 2400,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        MUX4.flush()
        if not MUX4.isOpen():
            logger.error("Com14 cannot be openned\r")
        #MUX4.close()
        MUX5 = serial.Serial('COM15', baudrate= 2400,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        MUX5.flush()
        if not MUX5.isOpen():
            logger.error("Com15 cannot be openned\r")
        #MUX5.close()
        TM = serial.Serial('COM16', baudrate= 115200,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        TM.flush()
        if not TM.isOpen():
            logger.error("Com16 cannot be openned\r")
        #TM.close()
        DMM = serial.Serial('COM18', baudrate= 38400,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        DMM.flush()
        if not  DMM.isOpen():
            logger.error("Com18 cannot be openned\r")
        #DMM.close()
        SRC_DC = serial.Serial('COM19', baudrate= 9600,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
        SRC_DC.flush()
        if not  SRC_DC.isOpen():
            logger.error("Com19 cannot be openned\r")
        #SRC_DC.close()
        
        return MUX1, MUX2, MUX3, MUX4, MUX5, TM,  DMM,  SRC_DC
        
#print(ser.name) # check which port was really used >>> ser.write(b'hello') # write a string >>> ser.close() # close port
#print(SRC_DC.name) 

#SRC_DC.write(b"SOUT1\r")
#d=doRead(SRC_DC,'\r')
#print(d)

# Initialize CVI's external devices
def InitExternalDevices(SRC_DC,  DMM,  TM):
    
    serialString1 = ""   # Used to hold data coming over UART
    serialString2 = ""
    serialString3 = ""
    #Ser_DC.write(b'Hello') # write a string

    while True:
    #    if stop_event.is_set(): return
    #    print("While LOOP STARTS\n")
        SRC_DC.write(b"SOUT1\r")
        
        
        time.sleep(0.01)
        # Wait until there is data waiting in the serial buffer
        if(SRC_DC.in_waiting > 0):                  #SOURCE-POWER DC SUPPLY (SRC_DC)         
                # Read data out of the buffer until a carraige return / new line is found
            #        time.sleep(1)
            serialString1 = ""
            SRC_DC.flush()
            serialString1 = SRC_DC.readline()
            
                # Print the contents of the serial data
#            try:
#                print(serialString1.decode('Ascii'))
#            except:
#                pass
            if serialString1 == b"OK\r":
                # Inform the device connected POWER SUPPLY over the serial port COM19 that we recevied the message string
                # The b at the beginning is used to indicate bytes
                print("Thank you for sending the message\n")
                SRC_DC.write(b"VOLT240\r")  #U out = 24.0V
                SRC_DC.write(b"CURR100\r")  #I out max = 1A
                SRC_DC.write(b"SOUT0\r")
                
                print("Initialization of SRC_DC finished!\n")
            else:
                print("SRC_DC is OFF or not connected.")
                break
        
        serialString2 = ""
        DMM.write(b"*IDN?\r")
                
        if(DMM.in_waiting > 0):                     # DIGITAL MULTI-METER (DMM)
                
                serialString2 = DMM.read(6)
#                try:
#                    print(serialString2.decode('Ascii'))
#                except:
#                    pass 
                    
                if serialString2 == b'*IDN?\r':
                    # Inform the device connected POWER SUPPLY over the serial port COM19 that we recevied the message string
                    # The b at the beginning is used to indicate bytes
                    print("Thank you for sending the message \r\n")
                    serialString2 = DMM.read(5)
#                    try:
#                        print(serialString2.decode('Ascii'))
#                    except:
#                        pass 
                    if (serialString2 == b'2831E'):
                        DMM.write(b":DISPLAY:ENABLE 1;:TRIG:SOURCE IMMEDIATE\r") #Default, immediate measurement 
                        print("Initialization of DMM finished!\n")
                    else:
                        print("Cannot found DMM")
                        break
                else:
                    print("DMM is OFF or not connected.")
                    
            #               if (serialString2== b"*IDN?\r2831E Multimeter,Ver1.4.14.06.18,123D15277\n"):
        
        serialString3 = ""
        TM.flush()
        TM.write(b"RAZ\r")
        
        if(TM.in_waiting > 0): 
            # TWIN MODEM (TM)
            serialString3 = ""
            TM.flush()
            serialString3 = TM.readline()
            print("Thank you for sending the message \r\n")
#            try:
#                print(serialString3.decode('Ascii'))
#                
#            except:
#                pass
                
            serialString3 = serialString3[0:23]
            if (serialString3 == b'1\r\rRADICOS Technologies'):
                TM.flush()
                #Wait all message received
                
                
                TM.write(b"par,7,5\r") #Set modem NB scan nodes = 7
                TM.write(b"wpar\r")   #Save par
                TM.write(b"mcf,9,0\r")
                print("Initialization of TM finished!\n")
                return
#                if (CheckCmdExecOK() == 0):
#                    TM.flush()
#                    TM.write(b"wpar\r")   #Save par
#                    time.sleep(0.1) #DelayWithEventProcessing(4);
#                	TM.flush()
#                    TM.write(b"mcf,9,0\r")  #Set modem to send data only after RMEAS cmd    
#                    print("Cannot set mcf 9=0");
#                else:
#                    print("Cannot init MODEM");
#            
            else:
                print("Cannot found MODEM, maybe is not connected")
                break
            
if __name__ == '__main__':
    
    MUX1, MUX2, MUX3, MUX4, MUX5, TM,  DMM,  SRC_DC = USB_Init()
    InitExternalDevices(SRC_DC,  DMM,  TM)


#if __name__ == "__main__":
#    SRC_DC = serial.Serial('COM19', baudrate= 9600,  bytesize = 8, parity = serial.PARITY_NONE,  timeout = 2,  stopbits = serial.STOPBITS_ONE) # open serial port
#    SRC_DC.write(b"SOUT1\r")
#    d=doRead(SRC_DC,'\r')
#    print(d)


#def doRead(ser,term):
#    matcher = re.compile(term)    #gives you the ability to search for anything
#    tic     = time.time()
#    buff    = ser.read(128)
#    # you can use if not ('\n' in buff) too if you don't like re
#    while ((time.time() - tic) < 2) and (not matcher.search(buff)):
#       buff += ser.read(128)
#    return buff
