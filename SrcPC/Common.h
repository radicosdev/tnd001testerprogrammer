/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file common.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/

//---------------------------------------------------------------------------
//  useful macros

#define BYTE    unsigned char
#define WORD    unsigned short

// for debug within CVI environment
#define PRINT(a,b) {char x[256];sprintf(x,a,b);OutputDebugString(x);}

// added since CVI don't implement __isascii()
#define __isascii(a)    ((unsigned)(a)<0x80)

// From IAR application notes
#define BIN8(a,b,c,d,e,f,g,h) (BYTE)(a<<7 +b<<6 +c<<5 +d<<4 +e<<3 +f<<2 +g<<1 +h)
//---------------------------------------------------------------------------

#include "main.h"
#include "Globres.h"  
#include "TestPlan.h"
#include "TestProc.h"
#include "Datalog.h"
#include "Utils.h"

#include "P1_prod.h"
#include "P2_debug.h"
#include "P3_spc.h"
#include "P4_config.h"
#include "P5_help.h"
#include "Timer.h"

#include "io_gpib.h"
#include "io_rs232.h"
#include "io_usb.h"

#include "TwinModem.h"


