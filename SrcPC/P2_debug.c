/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file P2_debug.c
    Debug's display tab - For qualified personal only\n
    \n
    History:\n
      150107: Lang: Started\n
*/

#include <windows.h>
#include <ansi_c.h>
#include "common.h"

//---------------------------------------------------------------------------
// needed for refreshing test name labels on Testplan loading

int p2_TestButtLabel[50] =
{
    0,
    P2_DEBUG_LBL_TEST_1,    P2_DEBUG_LBL_TEST_2,
    P2_DEBUG_LBL_TEST_3,    P2_DEBUG_LBL_TEST_4,
    P2_DEBUG_LBL_TEST_5,    P2_DEBUG_LBL_TEST_6,
    P2_DEBUG_LBL_TEST_7,    P2_DEBUG_LBL_TEST_8,
    P2_DEBUG_LBL_TEST_9,    P2_DEBUG_LBL_TEST_10,
    P2_DEBUG_LBL_TEST_11,   P2_DEBUG_LBL_TEST_12,
    P2_DEBUG_LBL_TEST_13,   P2_DEBUG_LBL_TEST_14,
    P2_DEBUG_LBL_TEST_15,   P2_DEBUG_LBL_TEST_16,
    P2_DEBUG_LBL_TEST_17,   P2_DEBUG_LBL_TEST_18,
    P2_DEBUG_LBL_TEST_19,   P2_DEBUG_LBL_TEST_20,
    P2_DEBUG_LBL_TEST_21,   P2_DEBUG_LBL_TEST_22,
    P2_DEBUG_LBL_TEST_23,   P2_DEBUG_LBL_TEST_24,
    P2_DEBUG_LBL_TEST_25,   P2_DEBUG_LBL_TEST_26,
    P2_DEBUG_LBL_TEST_27,   P2_DEBUG_LBL_TEST_28,
    P2_DEBUG_LBL_TEST_29,   P2_DEBUG_LBL_TEST_30,
    P2_DEBUG_LBL_TEST_31,   P2_DEBUG_LBL_TEST_32,
    P2_DEBUG_LBL_TEST_33,   P2_DEBUG_LBL_TEST_34,
    P2_DEBUG_LBL_TEST_35,   P2_DEBUG_LBL_TEST_36,
    P2_DEBUG_LBL_TEST_37,   P2_DEBUG_LBL_TEST_38,
    P2_DEBUG_LBL_TEST_39,   P2_DEBUG_LBL_TEST_40,
    P2_DEBUG_LBL_TEST_41,   P2_DEBUG_LBL_TEST_42,
    P2_DEBUG_LBL_TEST_43,   P2_DEBUG_LBL_TEST_44,
    P2_DEBUG_LBL_TEST_45,   P2_DEBUG_LBL_TEST_46,
    P2_DEBUG_LBL_TEST_47,   P2_DEBUG_LBL_TEST_48,
    P2_DEBUG_LBL_TEST_49,   P2_DEBUG_LBL_TEST_50,
};

//---------------------------------------------------------------------------
//  Add a line into Debug log screen

int DbgLog(char *str)
{
	int Status;
	
    if (LogScreenEnable) 
	{
        Status=InsertTextBoxLine(p2_handle, P2_DEBUG_LOG_SCREEN, -1, str);
	    ProcessSystemEvents();
	}
    return 0;
}

//---------------------------------------------------------------------------

int __DbgRunTest(int num)
{
    char    buff[80];
    int     errcode;

    TestInProgress  = 1;
    LogScreenEnable = 1;
    LogFileEnable   = 0;
    sprintf(buff, "[ TEST #%d ]\r", num);
    DbgLog (buff);

    errcode = (TestFunction[num])();

    TestInProgress = 0;
    return errcode;
}

//---------------------------------------------------------------------------
//  Special "Start Test" button: no data log

int CVICALLBACK DbgTestAll_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        LogScreenEnable = 1;
        LogFileEnable   = 0;
        CVI_TestAll();
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgReloadTestPlan_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        LoadTestPlan();
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK ClearLogScreen_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        ResetTextBox(p2_handle, P2_DEBUG_LOG_SCREEN, "\0");
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
//  Not yet implemented

int CVICALLBACK SaveLogScreen_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest1_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(1);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest2_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(2);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest3_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(3);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest4_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(4);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest5_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(5);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest6_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(6);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest7_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(7);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest8_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(8);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest9_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(9);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest10_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(10);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest11_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(11);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest12_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(12);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest13_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(13);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest14_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(14);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest15_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(15);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest16_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(16);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest17_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(17);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest18_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(18);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest19_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(19);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest20_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(20);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest21_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(21);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest22_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(22);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest23_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(23);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest24_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(24);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest25_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(25);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest26_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(26);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest27_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(27);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest28_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(28);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest29_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(29);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest30_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(30);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest31_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(31);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest32_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(32);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest33_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(33);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest34_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(34);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest35_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(35);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest36_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(36);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest37_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(37);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest38_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(38);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest39_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(39);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest40_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(40);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------

int CVICALLBACK DbgTest41_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(41);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest42_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(42);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest43_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(43);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest44_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(44);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest45_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(45);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest46_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(46);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest47_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(47);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest48_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(48);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest49_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(49);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
int CVICALLBACK DbgTest50_CB (int panel, int control, int event,
        void *callbackData, int eventData1, int eventData2)
{
    switch (event)
    {
    case EVENT_COMMIT:
        if (TestInProgress == 0)
            __DbgRunTest(50);
        break;
    }
    return 0;
}

//---------------------------------------------------------------------------
