/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH1031Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file p1_prod.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/

#define TEST_ACTIVE_COLUMN  1
#define TEST_ACTION_COLUMN  2
#define TEST_MINVAL_COLUMN  3
#define TEST_MAXVAL_COLUMN  4 
#define TEST_MEASURE_COLUMN 5
#define TEST_VERDICT_COLUMN 6
#define TEST_REMARK_COLUMN  7
#define NUMBER_COLUMNS      7


//---------------------------------------------------------------------------

void Init_TestTable(void);
void Update_TestTable(void);
void Clear_TestResults(void);
void Clear_BoardsTestResults(void);
void Update_TestResult(int i);
void Init_TestSummary(void);
void Update_TestSummary(void);
void SetFirstVisibleRow(int FirstR);


//---------------------------------------------------------------------------

