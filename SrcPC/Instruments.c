/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Instruments.c
    Specific measurement helpers, only for TestProc.c
    \n
    History:\n
      160207: Lang: Started\n
*/

#include <ansi_c.h>
#include "common.h"

//---------------------------------------------------------------------------

double HP_GetFloat(void)
{
    char MeasBuff[20]="";

/*

    for (i = 0; i <= 16; i++)
    {
        MeasBuff[i] = GetRxChar();
    }
    MeasBuff[i] = 0;
*/
    return (atof(MeasBuff));
}

//---------------------------------------------------------------------------

double HP_ReadFloat(void)
{
    char MeasBuff[40]="";
/*

    char        *ptr;
    int     i;

    // get header
    if (GetRxChar() != '#')
        return 0;

    // get length of "length" field
    MeasBuff[0] = GetRxChar();
    MeasBuff[1] = 0;
    i = atoi(MeasBuff);

    // get length of float value
    ptr = MeasBuff;
    for (; i > 0; i--)
    {
        *ptr++ = GetRxChar();
    }
    *ptr = 0;
    i = atoi(MeasBuff);

    // now get the float value
    ptr = MeasBuff;
    for (; i > 0; i--)
    {
        *ptr++ = GetRxChar();
    }
*/
    return (atof(MeasBuff));
}




