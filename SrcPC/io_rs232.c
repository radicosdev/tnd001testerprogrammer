/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file io_rs232.c
    *** NOT TESTED ***\n
    \n
    History:\n
      150107: Lang: Started\n
	  150714: PhW:  PePo\n
*/

#include <windows.h>
#include <utility.h>
#include <ansi_c.h>
#include <rs232.h>
#include "Common.h"
#include "TestProc.h"


//---------------------------------------------------------------------------
//  For rs232 data communication

#define RS_BUFSIZ   256

int             com11_port=11;
char            com11_RxBuff [RS_BUFSIZ +1];
unsigned char   com11_RxCnt;
char*           com11_RxPutPtr;
char*           com11_RxGetPtr;
int             com11_TimeOut;

int             com12_port=12;

int             com13_port=13;

int             com14_port=14;

int             com15_port=15;

int             com16_port=16;

int             com18_port=18;

int             com19_port=19;


// NUMATO1  com11_port
// NUMATO2  com12_port
// NUMATO3  com13_port
// NUMATO4  com14_port
// NUMATO5  com15_port
// MODEM    com16_port
// PROG     com17_port  not in RS mode but in USB mode
// DMM      com18_port



//---------------------------------------------------------------------------
///  open comXX


int com11_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com11_port, "COM11", 2400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn on Hardware handshaking
	    SetCTSMode (com11_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com11_port, 1);  //2.0

	    /// Make sure Serial buffers are empty
	    FlushInQ  (com11_port);
	    FlushOutQ (com11_port);

	    /// install callback to receive any char
	    //InstallComCallback (com11_port, LWRS_RXCHAR, 0, 0, com11_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------

int com12_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com12_port, "COM12", 2400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn off Hardware handshaking
	    SetCTSMode (com12_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com12_port, 1); 

	    /// Make sure Serial buffers are empty
	    FlushInQ  (com12_port);
	    FlushOutQ (com12_port);

	    /// install callback to receive any char
	    //InstallComCallback (com12_port, LWRS_RXCHAR, 0, 0, com12_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------

int com13_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com13_port, "COM13", 2400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn off Hardware handshaking
	    SetCTSMode (com13_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com13_port, 1); 
          
	    /// Make sure Serial buffers are empty
	    FlushInQ  (com13_port);
	    FlushOutQ (com13_port);

	    /// install callback to receive any char
	    //InstallComCallback (com13_port, LWRS_RXCHAR, 0, 0, com13_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------

int com14_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com14_port, "COM14", 2400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn off Hardware handshaking
	    SetCTSMode (com14_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com14_port, 1); 
          
	    /// Make sure Serial buffers are empty
	    FlushInQ  (com14_port);
	    FlushOutQ (com14_port);

	    /// install callback to receive any char
	    //InstallComCallback (com14_port, LWRS_RXCHAR, 0, 0, com14_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------

int com15_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com15_port, "COM15", 2400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn off Hardware handshaking
	    SetCTSMode (com15_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com15_port, 1); 
          
	    /// Make sure Serial buffers are empty
	    FlushInQ  (com15_port);
	    FlushOutQ (com15_port);

	    /// install callback to receive any char
	    //InstallComCallback (com15_port, LWRS_RXCHAR, 0, 0, com15_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------

int com16_open(void)
{
    int RetCode;
	int	TheOldvalue;

    /// Open and Configure MODM port
	TheOldvalue=SetBreakOnLibraryErrors(0);
	RetCode = OpenComConfig (com16_port, "COM16", 115200, 0, 8, 1, 512, -1);
	SetBreakOnLibraryErrors(TheOldvalue);
	ProcessSystemEvents ();
	if (RetCode < 0)
		return -1;
    SetCTSMode (com16_port, LWRS_HWHANDSHAKE_OFF);   // LWRS_HWHANDSHAKE_CTS_RTS  LWRS_HWHANDSHAKE_OFF
    SetComTime (com16_port, StdModemCommTimeOut); 
    /// Make sure Serial buffers are empty
    FlushInQ  (com16_port);
    FlushOutQ (com16_port);

    /// install callback to receive any char
    //InstallComCallback (com16_port, LWRS_RXCHAR, 0, 0, com16_Rx_CB, 0);
    return 0;
}

int com18_open(void)
{
    /// Open and Configure Com port
    if (OpenComConfig (com18_port, "COM18", 38400, 0, 8, 1, 512, -1) <= -1)
	{
		return -1;
	}
	else
	{
	    /// Turn off Hardware handshaking
	    SetCTSMode (com18_port, LWRS_HWHANDSHAKE_OFF);
	    SetComTime (com18_port, 1); 
          
	    /// Make sure Serial buffers are empty
	    FlushInQ  (com18_port);
	    FlushOutQ (com18_port);

	    /// install callback to receive any char
	    //InstallComCallback (com18_port, LWRS_RXCHAR, 0, 0, com18_Rx_CB, 0);
	    return 0;
	}
}

//---------------------------------------------------------------------------
///  shutdown comXX stream
int com_close(int TheCom)
{
	int	TheOldvalue;
	TheOldvalue=SetBreakOnLibraryErrors(0);
    CloseCom (TheCom);
	SetBreakOnLibraryErrors(TheOldvalue);
	ProcessSystemEvents ();
	//be shure com removed
	ProcessSystemEvents ();
	return 0;
}

//---------------------------------------------------------------------------
///  send a char over com11

int com11_TransmitChar(char TxChar)
{
    return ComWrt(com11_port, &TxChar, 1);
}

//---------------------------------------------------------------------------
///  receive one char from com11 (through ring buffer)

int com11_ReceiveChar(void)
{
    int RxChar = 0;

    /// wait for char within timeout
    Timer1_WatchDog2 = 100;     // 10 secs timeout
    while(com11_RxCnt == 0)
    {
        if (Timer1_WatchDog2)
            __MyPumpMessage();
        else
            return -1;
    }

    /// update ring buffer with received char
    RxChar = *com11_RxGetPtr;
    if (++com11_RxGetPtr > (com11_RxBuff +256))
        com11_RxGetPtr = com11_RxBuff;
    com11_RxCnt--;

    return RxChar;
}

//---------------------------------------------------------------------------
///  Callback function called when a char is received

void CVICALLBACK com11_Rx_CB (int portNo,int eventMask,void *callbackData)
{
    char    rxChar;

    if (GetInQLen(portNo) <= 0)
        return;

    while(1)
    {
        /// get char from communication stream
        if (ComRd (portNo, &rxChar, 1) <= 0)    // timeout
            return;

        /// put it into ring buffer
        *com11_RxPutPtr = rxChar;

        /// update pointer & counter accordingly
        if (++com11_RxPutPtr > (com11_RxBuff +256))
            com11_RxPutPtr = com11_RxBuff;
        com11_RxCnt++;
    }
}

//---------------------------------------------------------------------------










