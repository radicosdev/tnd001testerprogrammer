// ==========================================================================
//  MakeRef v3.02 (Oct 2009)
//  (C)1998-2009, L&V Design - www.lvdesign.ch
//
//  Generated from: globres.c
//  ---- DO NOT EDIT ---- DO NOT EDIT ---- DO NOT EDIT ---- DO NOT EDIT ---- 
// ==========================================================================

/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Globres.c
extern Declares all global variables here\n
    \n
extern History:\n
      150107: Lang: Started\n
	  150714: PhW:  PePo\n
*/

//---------------------------------------------------------------------------

extern char    TestExecDir[];

//---------------------------------------------------------------------------
// For user interface

int     mainPanel_handle;
int     p1_handle;
int     p2_handle;
int     p3_handle;
int     p4_handle;
int     p5_handle;

char    ProductID   [15];
char    TestPlanID  [15];
char    BatchNumber[10];
char	TestLogFilePath[256];
char    LogFileEXT  [10];
char    SerialNum   [15];
char	FirmwarePath[256];
char	ProgCmdPath[256];
char	ProgCmdLogFileName[20];



//---------------------------------------------------------------------------
//  for timing purpose (timeout, watchdog....)

int     Timer1_WatchDog1;
int     Timer1_WatchDog2;
int     Timer1_WatchDog3;
int     Timer1_WatchDog4;
int     Timer2_TestTime;

//---------------------------------------------------------------------------
//  for gpib interface

#define GPIB_ADDR_DEV1      11
//#define GPIB_ADDR_DEV2
//#define GPIB_ADDR_DEV3
//#define GPIB_ADDR_DEV4

int     gpib_dev1;
int     gpib_dev2;
int     gpib_dev3;
int     gpib_dev4;

#define GPIB_BUFSIZ 256
char    gpib_RxBuff[GPIB_BUFSIZ +1];

//---------------------------------------------------------------------------
//  For test processes

#define DEBUG_OFF   0
#define DEBUG_ON    1
//To save loop count info and testing time with TestResultText
int		LogWithAddOnInfos;
//To enavle extra control for debuging
int		EnableExtraCtrl;


int		LogScreenEnable;
int		LogFileEnable;
int		AbortTest;
int		TestInProgress;
int		BoardInProcess;
char	AllTestInError[50];
char	FirstValInError[20];
int		SerieIdToPrg;  //default buton position (off)
char	NodeId[3];

enum	AllTestResultType
{
	TEST_RUNNING    = -1,
	TEST_PASSED     = 0,
	TEST_FAILED     = 1,
	TEST_ABORTED	= 2
};
enum	AllTestResultType     AllTestsResult;

#define MAX_TEST        34
#define MAX_BOARD		8

enum	TestStateType
{
	TEST_HIDDEN     = -1,
	TEST_SKIPPED    = 0,
	TEST_NONFATAL   = 1,
	TEST_FATAL      = 2,
	TEST_SKIPPED_NOMINMAX   = 10,
	TEST_NONFATAL_NOMINMAX  = 11,
	TEST_FATAL_NOMINMAX     = 12
};

enum TestStateType TestState    [MAX_TEST +1];

enum	TestResultType
{
	ERR_NOT_INIT	= -3,
	ERR_HIDDEN 		= -2,
	ERR_SKIPPED 	= -1,
	ERR_NONE        = 0,
	ERR_NONFATAL    = 1,
	ERR_FATAL       = 2
};

enum	TestResultType    	TestResult      [MAX_TEST +1];

char    	TestName        [MAX_TEST +1][100];
char    	TestRemark      [MAX_TEST +1][100];
float   	TestMinVal      [MAX_TEST +1];
float   	TestMaxVal      [MAX_TEST +1];
float   	TestMeasVal     [MAX_TEST +1];
double   	TestTime	    [MAX_TEST +1];

char		TestResultTxt	[MAX_TEST +1][150];


//---------------------------------------------------------------------------


