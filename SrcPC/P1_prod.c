/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file P1_prod.c
    Production test's display tab\n
    \n
    History:\n
      150107: Lang: Started\n
*/

#include <windows.h>
#include <formatio.h>
#include <ansi_c.h>
#include "Common.h"

//---------------------------------------------------------------------------

void Init_TestTable(void)
{
    int     i;

    for (i = 1; i <= MAX_TEST; i++)
    {
        TestMinVal  	[i]		= 0.0;
        TestMeasVal 	[i]		= -1;
        TestMaxVal  	[i]		= 0.0;
        TestResult  	[i]		= ERR_HIDDEN;
        TestRemark  	[i][0]	= 0;
        TestName    	[i][0]	= 0;
        TestState   	[i]		= 0;
		TestResultTxt	[i][0]	=0;
		TestTime	    [i] 	=0.0;
    }
}

//---------------------------------------------------------------------------

void Update_TestTable(void)
{
    int     i;
    char    buff[40];

    for (i = 1; i <= MAX_TEST; i++)
    {
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_ACTIVE_COLUMN, i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_ACTION_COLUMN, i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MINVAL_COLUMN, i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MAXVAL_COLUMN, i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MEASURE_COLUMN,i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN,i), "");
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_REMARK_COLUMN, i), "");
        //SetCtrlVal      (p2_handle, p2_TestButtLabel[i], "");
    }

    for (i = 1; i <= MAX_TEST; i++)
    {
        // TEST_ACTIVE_COLUMN  1
        switch(TestState[i])
        {
        case TEST_SKIPPED:
        case TEST_SKIPPED_NOMINMAX:
            sprintf(buff, "Skipped");
            break;

        case TEST_NONFATAL:
        case TEST_NONFATAL_NOMINMAX:
            sprintf(buff, "NonFatal");
            break;

        case TEST_FATAL:
        case TEST_FATAL_NOMINMAX:
            sprintf(buff, "Fatal");
            break;

        default:
        case TEST_HIDDEN:
            continue;
        }
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_ACTIVE_COLUMN, i), buff);

        // TEST_ACTION_COLUMN  2
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_ACTION_COLUMN, i), TestName[i]);
        //SetCtrlVal      (p2_handle, p2_TestButtLabel[i], TestName[i]);

        // TEST_MINVAL_COLUMN 3
        if (TestState[i] >= TEST_SKIPPED_NOMINMAX)  sprintf(buff, "");
        else
		{	
		   	if (TestMinVal[i] >= 0.01)
			{
				sprintf(buff, "%3.3f", TestMinVal[i]);
			}
			else
			{
				sprintf(buff, "%.2e", TestMinVal[i]);
			}
        	SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MINVAL_COLUMN, i), buff);
		}

        // TEST_MEASURE_COLUMN 4
        if (TestState[i] >= TEST_SKIPPED_NOMINMAX)  sprintf(buff, "");
        else
		{
		   	if (TestMaxVal[i] >= 0.01)
			{
				sprintf(buff, "%3.3f", TestMaxVal[i]);
			}
			else
			{
				sprintf(buff, "%.2e", TestMaxVal[i]);
			}
        	SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MAXVAL_COLUMN, i), buff);
		}

        // TEST_MAXVAL_COLUMN  5
	   	if (TestMeasVal[i] >= 0.01)
		{
			sprintf(buff, "%3.3f", TestMeasVal[i]);
		}
		else
		{
			sprintf(buff, "%.2e", TestMeasVal[i]);
		}
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MEASURE_COLUMN, i), buff);

        // TEST_VERDICT_COLUMN 6
        switch(TestResult[i])
        {
        	case ERR_NOT_INIT:  sprintf(buff, "-");      break;
			case ERR_HIDDEN:    sprintf(buff, "");      break;
			case ERR_SKIPPED:   sprintf(buff, "SKIPPED");      break;
	        case ERR_NONE:      sprintf(buff, "PASS");  break;
	        case ERR_NONFATAL:  sprintf(buff, "NOT FATAL");  break;
	        case ERR_FATAL:     sprintf(buff, "FAIL");  break;
        }
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN, i), buff);

        // TEST_REMARK_COLUMN  7
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_REMARK_COLUMN, i), TestRemark[i]);
    }
}

//---------------------------------------------------------------------------

void Clear_TestResults(void)
{
    int     i;

    for (i = 1; i <= MAX_TEST; i++)
    {
        SetTableCellVal      (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MEASURE_COLUMN, i), "");
        SetTableCellVal      (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN, i), "");
        SetTableCellAttribute(p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN, i), ATTR_TEXT_BGCOLOR, VAL_WHITE);
        // Refresh to init remark
        SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_REMARK_COLUMN, i), TestRemark[i]);
    }

}

//---------------------------------------------------------------------------
	
	void Clear_BoardsTestResults(void)
{
    int     i;
	    for (i = 1; i <= MAX_BOARD; i++)
    {
        SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(i, 1), "");  //row 1 = Pass/Fail
        SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(i, 2), "");
        SetTableCellVal      (p1_handle, P1_PROD_BOARD_TABLE, MakePoint(i, 3), "");
        SetTableCellAttribute(p1_handle, P1_PROD_BOARD_TABLE, MakePoint(i, 1), ATTR_TEXT_BGCOLOR, VAL_WHITE);
    }

}

//---------------------------------------------------------------------------

void Update_TestResult(int i)
{
    char            buff[40];
    unsigned long   BackColor=VAL_WHITE ;
	char			RemarkResult[150];

    // TEST_MEASVAL_COLUMN  5
   	if (TestMeasVal[i] >= 0.0001)
	{
		sprintf(buff, "%3.3f", TestMeasVal[i]);
	}
	else
	{
		sprintf(buff, "%.2e", TestMeasVal[i]);
	}
    SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_MEASURE_COLUMN, i), buff);

    // TEST_VERDICT_COLUMN 6
    switch(TestResult[i])
    {
	    case ERR_NOT_INIT:  
			sprintf(buff, "");
	        BackColor = VAL_WHITE;
			break;
	    case ERR_HIDDEN:
	        sprintf(buff, "");
	        BackColor = VAL_WHITE;
	        break;
	    case ERR_SKIPPED:
	        sprintf(buff, "SKIPPED");
	        BackColor = VAL_WHITE;
	        break;

	    case ERR_NONE:
	        sprintf(buff, "PASS");
	        BackColor = VAL_GREEN;
	        break;

	    case ERR_NONFATAL:
	        sprintf(buff, "FAIL");
	        BackColor = VAL_YELLOW;
	        break;

	    case ERR_FATAL:
	        sprintf(buff, "FAIL");
	        BackColor = VAL_RED;
	        break;
    }
    SetTableCellAttribute(p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN, i), ATTR_TEXT_BGCOLOR, BackColor);
    SetTableCellVal      (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_VERDICT_COLUMN, i), buff);
    // TEST_REMARK_COLUMN  7
	sprintf(RemarkResult,"%s%s",TestRemark[i],TestResultTxt[i]); 
    SetTableCellVal (p1_handle, P1_PROD_TEST_TABLE, MakePoint(TEST_REMARK_COLUMN, i), RemarkResult);
}

//---------------------------------------------------------------------------

void Init_TestSummary(void)
{
    SetCtrlAttribute (p1_handle, P1_PROD_TEST_STS, ATTR_TEXT_BGCOLOR, VAL_WHITE);
    SetCtrlVal       (p1_handle, P1_PROD_TEST_STS, "Test in progress...");
}

//---------------------------------------------------------------------------

void Update_TestSummary(void)
{
    char    buff [65];
    char    buff2[10];
    int     i;

    switch(AllTestsResult)
    {
		case TEST_PASSED:
        sprintf(buff, "PASS");
	 	sprintf(AllTestInError,"No Error");
        break;

		case TEST_ABORTED:
	        sprintf(buff, "ABORT");
		 	sprintf(AllTestInError,"Interrupted tests");
			break;
	    
		case TEST_FAILED:
	    default:
	        sprintf(buff, "FAIL");
	 		sprintf(AllTestInError,"");
	        for(i = 1; i <= MAX_TEST; i++)
	        {
	            if (strlen(buff) < 55)
	            {
	                if (TestResult[i] >= ERR_NONFATAL)
	                {
	                    sprintf(buff2, ": test%d", i);
						sprintf(AllTestInError,"%s%d / ",AllTestInError, i);
						sprintf(FirstValInError,"%f", TestMeasVal[i]);
	                    strcat(buff, buff2);
	                }
	            }
	            else
	            {
	                strcat(buff, " ...");
	                break;
	            }
	        }
	        break;
    }

    SetCtrlAttribute (p1_handle, P1_PROD_TEST_STS, ATTR_TEXT_BGCOLOR, VAL_WHITE);
    SetCtrlVal       (p1_handle, P1_PROD_TEST_STS, buff);
}

//---------------------------------------------------------------------------

void SetFirstVisibleRow(int FirstR)
{
    SetCtrlAttribute (p1_handle, P1_PROD_TEST_TABLE, ATTR_FIRST_VISIBLE_ROW, FirstR);
	ProcessSystemEvents ();
}
