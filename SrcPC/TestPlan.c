/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file TestPlan.c
    Load external TestPlan (in INI format, text editable)\n
    \n
    History:\n
      150107: Lang: Started\n
	  191220: PW: update for AMM01TP
*/

#include <windows.h>
#include <utility.h>
#include <formatio.h>
#include <ansi_c.h>
#include "common.h"

//---------------------------------------------------------------------------
/// Load TestPlan

int LoadTestPlan(void)
{
    int     i;
    char    buff        [256];
    char    SectionName [50];
    char    TestPlanFile[260];
    extern void Update_MainParams(void);
	GetProjectDir(TestPlanFile);
    sprintf(TestPlanFile, "%s/Testplan.ini", TestPlanFile);
    sprintf(TestLogFilePath, "");

    // get configuration
    GetPrivateProfileString ("Setup", "ProductID",  (LPSTR) "unknown",
                             (LPSTR) ProductID, sizeof(ProductID), TestPlanFile);

    GetPrivateProfileString ("Setup", "TestPlanID", (LPSTR) "unknown",
                             (LPSTR) TestPlanID, sizeof(TestPlanID), TestPlanFile);

    GetPrivateProfileString ("Setup", "TestLogFilePath", (LPSTR) "unknown",
                             (LPSTR) TestLogFilePath, sizeof(TestLogFilePath), TestPlanFile);

    GetPrivateProfileString ("Setup", "LogFileEXT", (LPSTR) "unknown",
                             (LPSTR) LogFileEXT, sizeof(LogFileEXT), TestPlanFile);

    GetPrivateProfileString ("Setup", "LogWithAddOnInfos", (LPSTR) "0",
                             (LPSTR) buff, sizeof(buff), TestPlanFile);
	LogWithAddOnInfos = atoi(buff);

    GetPrivateProfileString ("Setup", "EnableExtraCtrl", (LPSTR) "0",
                             (LPSTR) buff, sizeof(buff), TestPlanFile);
	EnableExtraCtrl = atoi(buff);

    GetPrivateProfileString ("Setup", "FirmwarePath", (LPSTR) "unknown",
                             (LPSTR) FirmwarePath, sizeof(FirmwarePath), TestPlanFile);

    GetPrivateProfileString ("Setup", "ProgCmdPath", (LPSTR) "unknown",
                             (LPSTR) ProgCmdPath, sizeof(ProgCmdPath), TestPlanFile);

    GetPrivateProfileString ("Setup", "ProgCmdLogFileName", (LPSTR) "unknown",
                             (LPSTR) ProgCmdLogFileName, sizeof(ProgCmdLogFileName), TestPlanFile);

    // get test definitions
    Init_TestTable();
	sprintf(DescriptLineTests, "Date,Time,Result,SSN,Board Nu"); 
    for (i = 1; i <= MAX_TEST; i++)
    {
        sprintf(SectionName, "Test%d", i);

        // Get state (disabled, non-fatal, fatal)
        GetPrivateProfileString (SectionName, "State", (LPSTR) "0",
                                 (LPSTR) buff, sizeof(buff), TestPlanFile);
        TestState[i] = atoi(buff);

        // test name
        GetPrivateProfileString (SectionName, "TestName", (LPSTR) "",
                                 (LPSTR) buff, sizeof(buff), TestPlanFile);
        sprintf(TestName[i], buff);
		
		sprintf(DescriptLineTests, "%s,%d-%s",DescriptLineTests,i,buff );

        // get minimum value
        GetPrivateProfileString (SectionName, "MinVal", (LPSTR) "0.0",
                                 (LPSTR) buff, sizeof(buff), TestPlanFile);
        TestMinVal[i] = (float)atof(buff);

        // get minimum value
        GetPrivateProfileString (SectionName, "MaxVal", (LPSTR) "0.0",
                                 (LPSTR) buff, sizeof(buff), TestPlanFile);
        TestMaxVal[i] = (float)atof(buff);

        // test remark
        GetPrivateProfileString (SectionName, "Remark", (LPSTR) "",
                                 (LPSTR) buff, sizeof(buff), TestPlanFile);
		
        sprintf(TestRemark[i], buff);
    }

	sprintf(DescriptLineTests, "%s,Tests In Error\r",DescriptLineTests ); 
    Update_MainParams();
    Update_TestTable();
    Clear_TestResults();
    return 0;
}

//---------------------------------------------------------------------------

