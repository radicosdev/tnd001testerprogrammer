/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file io_gpib.c
    \n
    History:\n
      150107: Lang: Started\n
*/

#include <windows.h>
#include <utility.h>
#include <formatio.h>
#include <ansi_c.h>
#include "gpib.h"
#include "common.h"

//---------------------------------------------------------------------------

int gpib_open(void)
{
    DisableBreakOnLibraryErrors();

#ifdef GPIB_ADDR_DEV1
    gpib_dev1 = ibdev(0, GPIB_ADDR_DEV1, NO_SAD, T10s, 1, 0);
    if (ibsta & 0x8000)
    {
        MessageBox(NULL, TEXT("Cannot initialize GPIB_dev1"), TEXT("GPIB error"), MB_OK);
        return -1;
    }
#endif

#ifdef GPIB_ADDR_DEV2
    gpib_dev2 = ibdev(0, GPIB_ADDR_DEV2, NO_SAD, T10s, 1, 0);
    if (ibsta & 0x8000)
    {
        MessageBox(NULL, TEXT("Cannot initialize GPIB_dev2"), TEXT("GPIB error"), MB_OK);
        return -1;
    }
#endif

#ifdef GPIB_ADDR_DEV3
    gpib_dev3 = ibdev(0, GPIB_ADDR_DEV3, NO_SAD, T10s, 1, 0);
    if (ibsta & 0x8000)
    {
        MessageBox(NULL, TEXT("Cannot initialize GPIB_dev3"), TEXT("GPIB error"), MB_OK);
        return -1;
    }
#endif

#ifdef GPIB_ADDR_DEV4
    gpib_dev4 = ibdev(0, GPIB_ADDR_DEV4, NO_SAD, T10s, 1, 0);
    if (ibsta & 0x8000)
    {
        MessageBox(NULL, TEXT("Cannot initialize GPIB_dev4"), TEXT("GPIB error"), MB_OK);
        return -1;
    }
#endif

    return 0;
}

//---------------------------------------------------------------------------

int gpib_close(void)
{
    return 0;
}

//---------------------------------------------------------------------------

int gpib_write(int dev, char *cmdstr)
{
    ibwrt(dev, cmdstr, strlen(cmdstr));
    if (ibsta & 0x8000)
    {
        // more error handling needed
        return -1;
    }
    return 0;
}

//---------------------------------------------------------------------------

int gpib_read(int dev)
{
    FillBytes(gpib_RxBuff, 0, GPIB_BUFSIZ, 0x0);
    ibrd(dev, gpib_RxBuff, GPIB_BUFSIZ);
    if (ibsta & 0x8000)
    {
        // more error handling needed
        return -1;
    }
    return 0;
}

//---------------------------------------------------------------------------

