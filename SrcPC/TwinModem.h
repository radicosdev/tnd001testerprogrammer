/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file TwinModem.h
    \n
    History:\n
      191220: PhW: Started\n
*/


int CheckId(char ResultStr[50]);
int CheckScanRaw(char ResultStr[50]);
int SetOut18V(char ResultStr[50]);
int SetOut0V(char ResultStr[50]);
int SetOut9V(char ResultStr[50]);

int CheckCmdExecOK();
