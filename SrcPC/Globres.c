/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file Globres.c
    Init global variables here\n
    \n
    History:\n
      150107: Lang: Started\n
	  150714: PhW:  PePo\n
*/

//---------------------------------------------------------------------------


int		LogWithAddOnInfos 	= 0;
int		EnableExtraCtrl		= 0;
