#Initialize serial device
#import serial
#import serial.tools.list_ports

#import time
#import serial
import logging
 
# Create and configure logger
logging.basicConfig(filename="newfile.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
 
# Creating an object
logger = logging.getLogger()

# Setting the threshold of logger to DEBUG
logger.setLevel(logging.DEBUG)
 
import configparser
#import os
config = configparser.ConfigParser()

##Get the absolute path of ini file by doing os.getcwd() and joining it to config.ini
#ini_path = os.path.join(os.getcwd(),'TestPlan.ini')
#config.read(ini_path)
config.read('TestPlan.ini')
keys = [
    "ProductID",
    "TestPlanID",
    "TestLogFilePath",
    "LogFileEXT",
    "LogWithAddOnInfos",  
    "EnableExtraCtrl",
    "FirmwarePath",
    "ProgCmdPath", 
    "ProgCmdLogFileName"
]

keys1 = [
    "State", 
    "TestName", 
    "MinVal", 
    "MaxVal", 
    "Remark"
]
#
#ch2 = config.get('Test36','TestName')
#ch3 = config.get('Test37','TestName')
#ch4 = config.get('Test38','TestName')
#print(ch2,  ch3,  ch4)

confdict = {section: dict(config.items(section)) for section in config.sections()}
#print(confdict)
#print(confdict['Setup']['productid'])
#print(confdict['Test1'])
#print(confdict['Test2']['testname'])
from collections import OrderedDict
import matplotlib.pyplot as plt

a=[]
b=[]
for p_id, p_info in confdict.items():
    print("\nTEST:", p_id)
    a=p_id
    j=0
    for key in p_info:
        b=print(key + ':', p_info[key])
        
#for key, value in a_dictionary.items():
#        print (key, value)
#        #key, value = line.split() #Split line into a tuple
#        a_dictionary[key] = value #Add tuple values to dictionary
#
#for key in a_dictionary:
#    value = a_dictionary[key]
#    print(key, value)
#    
#print(a_dictionary)

#TO DO: make a dictionary to put all the values and define a max number of tests and after make a for loop to test that all these tests will be in the gui
#
#for key in keys:
#    try:
#        value1 = config.get("Setup", key)
#        print(f"{key}:", value1)
#    except configparser.NoOptionError:
#        print(f"No option '{key}' in section 'SETTINGS'")
#
#for key in keys1:
#    
#    try:
#        value2 = config.get("Test1", key)
#        print(f"{key}:", value2)
#    except configparser.NoOptionError:
#        print(f"No option '{key}' in section 'SETTINGS'")
#    try:
#        value3 = config.get("Test2", key)
#        print(f"{key}:", value3)
#    except configparser.NoOptionError:
#        print(f"No option '{key}' in section 'SETTINGS'")
#        
