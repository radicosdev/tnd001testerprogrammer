/////////////////////////////////////////////////////////////////////////////
//
//  Project: CVI_Test framework
//
//  Copyright L&V Design   - CH-1031 Mex
//
/////////////////////////////////////////////////////////////////////////////
/**
    @file TestProc.h
    \n
    History:\n
      150107: Lang: Started\n
      150612: PhW:  Updated to PePo\n
*/


const	double StdModemCommTimeOut;
const 	double	WaitTelitEndwriteScript;



#define NUMATO1  com11_port
#define NUMATO2  com12_port
#define NUMATO3  com13_port
#define NUMATO4  com14_port
#define NUMATO5  com15_port
#define MODEM    com16_port
#define DMM      com18_port


int CVI_TestInit(void);
int CVI_InitExternalDevices(void);
int CVI_TestCleanUp(void);
int CVI_Test1(void );
int CVI_Test2(void );
int CVI_Test3(void );
int CVI_Test4(void );
int CVI_Test5(void );
int CVI_Test6(void );
int CVI_Test7(void );
int CVI_Test8(void );
int CVI_Test9(void );
int CVI_Test10(void );
int CVI_Test11(void );
int CVI_Test12(void );
int CVI_Test13(void );
int CVI_Test14(void );
int CVI_Test15(void );
int CVI_Test16(void );
int CVI_Test17(void );
int CVI_Test18(void );
int CVI_Test19(void );
int CVI_Test20(void );
int CVI_Test21(void );
int CVI_Test22(void );
int CVI_Test23(void );
int CVI_Test24(void );
int CVI_Test25(void );
int CVI_Test26(void );
int CVI_Test27(void );
int CVI_Test28(void );
int CVI_Test29(void );
int CVI_Test30(void );
int CVI_TestAll(void);

int	ReplaceIDinName(char *inputstring, char *outputstring);

int send_com(int portNo, char str[]);


void SelectBoardForMeasurements(int BoardToSelect);

typedef int(*Ptr2Func)(void);
extern const Ptr2Func  TestFunction[MAX_TEST+1];


